const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let formsubmissionschema = new Schema({
    data: Schema.Types.Mixed
},{
    timestamps: true,
    strict: false
});

module.exports = mongoose.model('form_submission', formsubmissionschema);