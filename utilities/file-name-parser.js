exports.parseFileName = (route) => {

    if (!route || route === '') {
        return 'index';
    }

    if(route && route.includes('.html')){
        return route.split('.')[0];
    }

    return `${route}`;
}