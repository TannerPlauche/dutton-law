const MongoClient = require('mongodb').MongoClient;
const FormSubmission = require('../../models/form_submission.model');

class DBAccessor {
    // storage here
    //https://cloud.mongodb.com/v2#/org/5ee19a69a1921a373d88a3e5/projects
    // duttonlawga@gmail.com
    // DuttonLaw123
    constructor() {
        this.uri = "mongodb+srv://DuttonLaw123:DuttonLaw123@cluster0-rpntd.gcp.mongodb.net/dutton?useUnifiedTopology=true";
        const uri = "mongodb+srv://DuttonLaw123:<password>@cluster0-rpntd.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority";
        this.db = 'dutton';
        this.collection = 'form_submissions';
        this.config = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useNewUrlParser: true,
            ssl: false,
            checkServerIdentity: false,
        };
    }

    postData(data) {
        return new Promise((resolve, reject) => {

            let submission = new FormSubmission(data);
            submission.save((err, response) => {

                if (err) {
                    console.log('err', err);
                    reject(err);
                } else {
                    console.log(response);
                    resolve(response);
                }
            });

            // const client = new MongoClient(this.uri, this.config);
            // console.log('connecting to db');
            // client.connect(err => {

            //     if (err) {
            //         console.log('err', err);
            //         return;
            //     }

            //     const collection = client.db('dutton').collection(this.collection);
            //     console.log(collection);
            //     collection.insertOne({ data: 'test' }).then(response => {
            //         console.log(response);
            //         resolve(response);
            //         client.close();
            //     }).catch(err => {
            //         console.error(err);
            //         reject(err);
            //         client.close();
            //     });
            // });

        })

    }
}

// let db = new DBAccessor();
// db.postData({ test: 'test' })
//     .then(res => console.log(res))
//     .catch(err => console.log(err));

module.exports = DBAccessor;