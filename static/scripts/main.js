// (function () {
//     setBodyWidth();

//     window.addEventListener('resize', () => {
//         console.log('resize');
//         console.log(window.innerWidth);
//         setBodyWidth();
//     })


// })()

// function setBodyWidth() {
//     const body = document.getElementsByName('body');F
//     setTimeout(() => {
//         let width = window.screen.width;
//         let outerWidth = window.outerWidth;

//         document.body.style.width = `${width}px`;
//         console.log(width, outerWidth, document.body.style.width);
//     }, 1000);

// }
function onSubmit(token) {
    document.getElementById("contact-form").submit();
}

function onClick(event) {
    event.preventDefault();
    grecaptcha.ready(function () {
        grecaptcha.execute('6LekH6MZAAAAAM15TqXh0M2aDED00qr7ZTvxgsqf', {action: 'submit'})
            .then(function (token) {
                // Add your logic to submit to your backend server here.
                console.log(token);
                console.log('yo');
                axios.post('/form-submission', event)
                    .then(result => {
                        console.log(result);
                    })
            })
            .catch(err => {
                console.log(err);
            });
    });
}

function setUpNestedNavs() {

    // function setUpNestedNavs() {
    //     $('.nested-nav').on('mouseenter', () => {
    //         $(this).show();
    //         console.log('this');
    //     })
    //     document.querySelectorAll('.nested-nav')
    // }
    function displayNestedNav(navItem) {
        let children = Array.from(navItem.children);
        let nestedNav = children.find(child => child.classList.contains('nested-nav'));

        if (nestedNav) {
            nestedNav.style.visibility = 'inherit';
        }
    }

    function hideNestedNavItem(navItem) {
        let children = Array.from(navItem.children);
        let nestedNav = children.find(child => child.classList.contains('nested-nav'));

        if (nestedNav) {
            nestedNav.style.visibility = 'hidden';
        }
    }

    let nestedNavs = Array.from(document.querySelectorAll('.contains-nested-nav')).forEach(navItem => {
        navItem.addEventListener('mouseenter', () => displayNestedNav(navItem));

        navItem.addEventListener('mouseleave', () => hideNestedNavItem(navItem));

    });
    console.log(nestedNavs);
}

(function init() {
    setUpNestedNavs();
})();