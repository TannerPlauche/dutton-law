const mailgun = require("mailgun-js");
const DBAccessor = require("./database");

module.exports = class FormHandler {
    constructor() {

    }

    handleForm(req, res) {
        const formData = req.body;
        const phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        // email2 is honeypot field filled out by bots. The field is not visible
        if (formData.email2) {
            return res.redirect(req.headers.referer);
        }

        if (formData && formData.Name && formData.Name.includes('Henrysup')) {
            return res.redirect(req.headers.referer);
        }

        if (formData && formData.Email && !formData.Email.includes('@')) {
            return res.redirect(req.headers.referer);
        }

        if (formData && formData.email && !formData.email.includes('@')) {
            return res.redirect(req.headers.referer);
        }

        if (formData && formData.Phone && !phoneRegex.test(formData.Phone)) {
            return res.redirect(req.headers.referer);
        }

        const formJSON = this.createFormJSON(formData);
        const DOMAIN = "contact.duttonlawga.com";
        const mg = mailgun({ apiKey: '8c3704081fd57678c4442229e3109677-65b08458-d074d60b', domain: DOMAIN });
        const data = {
            from: 'duttonlawga@gmail.com',
            to: 'bridget@jamesrdutton.com, ashley@jamesrdutton.com, duttonlawga@gmail.com',
            subject: 'Contact Form Submitted From Dutton & Dutton website',
            text: JSON.stringify(formJSON),
            html: `<div> <pre> ${formJSON} </pre>  </div>`
        };


        this.logSubmission(formData)
        mg.messages().send(data, function (error, body) {
            if (error) {
                console.log('error', error);
            }
            res.redirect(req.headers.referer);
        });


    }


    logSubmission(data) {
        let dBAccessor = new DBAccessor();
        console.log('saving submission');

        if (data['Submit.x']) {
            delete data['Submit.x']
        }

        if (data['Submit.y']) {
            delete data['Submit.y']
        }

        return dBAccessor.postData(data);
        // .then(response => {
        //     console.log(response);
        // }).catch(err => {
        //     console.log(err);
        // });

    }

    createFormJSON(formData) {
        let json = JSON.stringify(formData);
        json = json.replace(/,/g, ', \n');
        return json;
    }

};
