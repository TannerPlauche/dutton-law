const express = require('express');
const app = express();
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const path = require('path');
const port = 8080;
const fileParser = require('./utilities/file-name-parser');
const FormHandler = require('./static/scripts/form-handler');
const bodyParser = require('body-parser');

mongoose.connect("mongodb+srv://DuttonLaw123:DuttonLaw123@cluster0-rpntd.gcp.mongodb.net/dutton?useUnifiedTopology=true");

app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');
app.set('trust proxy', true);

app.use(bodyParser.urlencoded({extended: false}))

app.use('/styles', express.static(path.join(__dirname, 'styles')));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/fonts', express.static(path.join(__dirname, 'fonts')));
app.use('/static', express.static(path.join(__dirname, 'static')));
app.use('/includes', express.static(path.join(__dirname, 'static')));
app.use('/design', express.static(path.join(__dirname, 'static')));
app.post('/form-submission', (req, res) => new FormHandler().handleForm(req, res));
app.use('/:filename', renderPage);
app.use('/', renderPage);

function renderPage(req, res) {
    console.log(req.baseUrl, req.url);
    try {

        if (req.baseUrl.includes('design') || req.baseUrl.includes('includes')) {
            return res.send(null);
        }

        // console.log('params', req.params);
        let fileName = fileParser.parseFileName(req.params.filename);
        console.log('filename', fileName);
        // res.render(path.join(__dirname, `pages/${fileName}`));
        res.render(fileName);
    } catch (err) {
        // console.log(err);
        res.send(`Error loading ${req.baseUrl + req.url}`);
    }
}


app.listen(port, () => console.log(`Server running on port ${port}`));