import re
import requests
from bs4 import BeautifulSoup
from urls import urls

def replaceDesignUrls(html):
    html = html.replace('/design/css/site.css', './site.css')
    html = html.replace('/design/images', './images')
    html = html.replace('/includes/images', './images')
    html = html.replace('.shtml', '.html')
    return html

def getHTML(site_url):
    response = requests.get(site_url)
    print(response)
    html = BeautifulSoup(response.text, "html.parser")
    html = str(html)
    html = replaceDesignUrls(html)
    return html


def getNewFileName(site_url):
    url_segments = site_url.split("/")
    file_name = url_segments[-1]
    root_file_name = ''

    if '.shtml' in file_name:
        file_name
        file_segments = file_name.split(".")
        print(file_segments)
        root_file_name = file_segments[0]
    else:
        print(url_segments)
        # file_segments = file_name.split('/')
        # print(file_segments)
        url_segments.reverse()
        root_file_name = url_segments[1]

    new_file_name = root_file_name + ".html"
    print('new file name: ', new_file_name)
    return new_file_name


def createFile(file_name, content):
    new_file = open(file_name, "a")
    new_file.write(content)
    new_file.close()




# site_url = 'https://www.jamesrdutton.com/Representative-Cases.shtml'
# urls = ['https://www.jamesrdutton.com/Criminal-Defense/', 'https://www.jamesrdutton.com/Traffic-Offenses/', 'https://www.jamesrdutton.com/Testimonials/Lamar-County-Criminal-Defense-Attorney.shtml']
for site_url in urls:

    html = getHTML(site_url)
    newFileName = getNewFileName(site_url)
    createFile(newFileName, html)
